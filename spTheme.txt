Simple:Press Theme Title: CSS-Only
Version: 2.0.1
Theme URI: https://simple-press.com
Description: A variant of the default theme showcasing standard CSS only - for those who are unable to use our php overlay styled themes
Author: Andy Staines, Steve Klasen and Brandon C
Author URI: https://simple-press.com/custom-simplepress-themes-for-every-need/
Stylesheet: css-only.css
Screenshot: spTheme.png
Simple:Press Versions: 6.3.0 and above